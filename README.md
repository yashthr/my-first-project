# My First Project

***Hello***
<!-- blank line -->
Hi

https://about.gitlab.com/handbook/markdown-guide/

 http://www.deadlinkchecker.com

[Text to display][identifier] will display a link.
<!-- Identifiers, in alphabetical order -->
[identifier]: http://example1.com

![Semantic description of image](https://images.unsplash.com/photo-1533495720786-6dade9109c5d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjc1MjQyfQ&auto=format&fit=crop&w=882&q=80 "Image Title")

<div class="panel panel-gitlab-orange">
**Heading**
{: .panel-heading}
<div class="panel-body">

Text in markdown.

</div>




```
def hello
   puts "Hello world!"
end
```


This is a paragraph
{::comment}
This is a comment which is
completely ignored.
{:/comment}
... paragraph continues here.



### <i class="fab fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> Purple GitLab Tanuki
{: #tanuki-purple}

### <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Orange GitLab Tanuki
{: #tanuki-orange}




